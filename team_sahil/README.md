# Steps
Install GitLab Runner on the VM instance.
Register the runner with your GitLab instance.
Install Java or python (sudo apt install openjdk-11-jre || python3.8)
Install Docker (sudo apt install docker.io)
Install SonarQube and configure it to run on http://:9000 [Don't worry detailed steps are provided].
Open the Inbound ports - 80, 443 and 9000
Open the Outbound ports - 80 and 443

# Open the following ports in VM under security groups: 
--SSH 22
--HTTPS 443
--HTTP 80
--Custom for node.js 3000


# To Configure a Sonar Server locally -> access at http://<ip address>:default port 9000, Java is also req to run sonarqube so install Java (apt install openjdk-11-jre), default username = admin, pass = admin
-> apt install unzip
-> adduser sonarqube
-> sudo su - sonarqube (to login to sonarqube user)
-> wget https://binaries.sonarsource.com/Distribution/sonarqube/sonarqube-9.4.0.54424.zip
-> unzip *
-> chmod -R 755 /home/sonarqube/sonarqube-9.4.0.54424
-> chown -R sonarqube:sonarqube /home/sonarqube/sonarqube-9.4.0.54424
-> cd sonarqube-9.4.0.54424/bin/linux-x86-64/ (place where sonar.sh start server script present)
-> ./sonar.sh start
-> ./sonar.sh status

-- systemctl --type=service --state=running (check all running servicess)

# after opening in browser, go to my accounts->settings & generate token to connect with GitLab (sqa_145c057f98e7f6f185aad25e39abb02652a38a90)

####